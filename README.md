# Nexa4j

This Java library include the following:
* Simple RPC client for Nexa node
* Nexa Address formatter

## Installation

Look here https://gitlab.com/otoplo/nexa-libs/nexa4j/-/releases for the latest package version.
In the pom.xml include the repository and dependency:
```
    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/api/v4/projects/44638913/packages/maven</url>
        </repository>
    </repositories>

    <dependency>
        <groupId>com.otoplo</groupId>
        <artifactId>nexa4j</artifactId>
        <version>{VERSION}</version>
    </dependency>
```

## Usage

RPC:
```
    public static void main(String[] args) {
        // Client init
        NexaRpcClient client = new NexaRpcClient("https://127.0.0.1:7227/", "user", "password");

        // operation
        Block block = client.getBlockInfo("hash_or_height");

        // raw operations in case the library not wrapped the required commad
        Block block1 = client.executeRequest("getblock", Block.class, "hash_or_height");
        JsonNode json = client.executeRequest("getrawtxpoolinfo", JsonNode.class, "hash_or_height");
    }
```

Address formatter:
```
    public static void main(String[] args) {
        AddressDecodedParts addressDecodedParts = AddressFormatter.decodeNexaAddress("nexa:nqtsq5g5402qrtfrhfd4uusvdgs0cal5r6g27auyy6cuuzxn");

        String address = AddressFormatter.toNexaAddress(addressDecodedParts.getAddressType(), addressDecodedParts.getHash(), NexaNetwork.MAIN);
        // the mainnet is default network
        String address2 = AddressFormatter.toNexaAddress(AddressType.P2PKT, addressDecodedParts.getHash());
    }
```
