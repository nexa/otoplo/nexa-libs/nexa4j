package com.otoplo.nexa4j.address;

public enum AddressType {

	P2PKH((byte) 0), P2SH((byte) 8), P2PKT((byte) 152), GP2PKT((byte) 88);

	private final byte versionByte;

	AddressType(byte versionByte) {
		this.versionByte = versionByte;
	}

	public byte getVersionByte() {
		return versionByte;
	}
}