package com.otoplo.nexa4j.rpc.jsonrpc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;

/**
 * Date: 8/9/14
 * Time: 9:04 PM
 * <p>Type-safe builder of JSON-RPC requests.</p>
 * <p>It introduces fluent API to build a request, set an expected response type and perform the request.
 * Builder is immutable: every mutation creates a new object, so it's safe to use
 * in multi-threaded environment.</p>
 * <p>It delegates JSON processing to Jackson {@link ObjectMapper} and actual request performing
 * to {@link Transport}.</p>
 */
public class RequestBuilder<T> {

    /**
     * JSON-RPC request method
     */
    private final String method;

    /**
     * JSON-RPC request id
     */
    private final ValueNode id;

    /**
     * JSON-RPC request params as a map
     */
    private final ObjectNode objectParams;

    /**
     * JSON-RPC request params as an array
     */
    private final ArrayNode arrayParams;

    /**
     * Generic type for representing expected response type
     */
    private final JavaType javaType;

    /**
     * Transport for performing a text request and returning a text response
     */
    private final Transport transport;

    /**
     * Jackson mapper for JSON processing
     */
    private final ObjectMapper mapper;

    /**
     * Creates a new default request builder without actual parameters
     *
     * @param transport transport for request performing
     * @param mapper    mapper for JSON processing
     */
    public RequestBuilder(Transport transport, ObjectMapper mapper) {
        this.transport = transport;
        this.mapper = mapper;
        id = NullNode.instance;
        objectParams = mapper.createObjectNode();
        arrayParams = mapper.createArrayNode();
        method = "";
        javaType = TypeFactory.defaultInstance().constructType(Object.class);
    }

    /**
     * Creates new builder as part of a chain of builders to a full-initialized type-safe builder
     *
     * @param transport    new transport
     * @param mapper       new mapper
     * @param method       new method
     * @param id           new id
     * @param objectParams new object params
     * @param arrayParams  new array params
     * @param javaType     new response type
     */
    private RequestBuilder(Transport transport, ObjectMapper mapper, String method,
                           ValueNode id, ObjectNode objectParams, ArrayNode arrayParams,
                           JavaType javaType) {
        this.transport = transport;
        this.mapper = mapper;
        this.method = method;
        this.id = id;
        this.objectParams = objectParams;
        this.arrayParams = arrayParams;
        this.javaType = javaType;
    }

    /**
     * Sets a request id as a long value
     *
     * @param id a  request id
     * @return new builder
     */
    public RequestBuilder<T> id(Long id) {
        return new RequestBuilder<>(transport, mapper, method, new LongNode(id), objectParams, arrayParams, javaType);
    }

    /**
     * Sets a request method
     *
     * @param method a request method
     * @return new builder
     */
    public RequestBuilder<T> method(String method) {
        return new RequestBuilder<>(transport, mapper, method, id, objectParams, arrayParams, javaType);
    }

    /**
     * Sets request parameters to request parameters.
     * Parameters are interpreted according to its positions.
     *
     * @param values array of parameters
     * @return new builder
     */
    public RequestBuilder<T> params(Object... values) {
        return new RequestBuilder<>(transport, mapper, method, id, objectParams, arrayParams(values), javaType);
    }

    /**
     * Sets expected return type. This method is suitable for non-generic types
     *
     * @param responseType expected return type
     * @param <NT>         new return type
     * @return new builder
     */
    public <NT> RequestBuilder<NT> returnAs(Class<NT> responseType) {
        return new RequestBuilder<>(transport, mapper, method, id, objectParams, arrayParams,
                TypeFactory.defaultInstance().constructType(responseType));
    }

    /**
     * Execute a request through {@link Transport} and convert a not null response to an expected type
     *
     * @return expected not null response
     * @throws JsonRpcException      in case of JSON-RPC error, returned by the server
     * @throws IllegalStateException if the response is null
     */
    public T execute() {
        T result = executeAndConvert();
        if (result == null) {
            throw new IllegalStateException("Response is null. Use 'executeNullable' if this is acceptable");
        }
        return result;
    }

    @Nullable
    private T executeAndConvert() {
        String textResponse = executeRequest();

        try {
            JsonNode responseNode = mapper.readTree(textResponse);
            JsonNode result = responseNode.get(RpcConstants.RESULT);
            JsonNode error = responseNode.get(RpcConstants.ERROR);
            JsonNode id = responseNode.get(RpcConstants.ID);

            /*
            JsonNode version = responseNode.get(JSONRPC);
            if (version == null) {
                throw new IllegalStateException("Not a JSON-RPC response: " + responseNode);
            }
            if (!version.asText().equals(VERSION_2_0)) {
                throw new IllegalStateException("Bad protocol version in a response: " + responseNode);
            }
             */
            if (id == null) {
                throw new IllegalStateException("Unspecified id in a response: " + responseNode);
            }

            if (error == null || error.isNull()) {
                if (result != null) {
                    return mapper.convertValue(result, javaType);
                } else {
                    throw new IllegalStateException("Neither result or error is set in a response: " + responseNode);
                }
            } else {
                ErrorMessage errorMessage = mapper.treeToValue(error, ErrorMessage.class);
                throw new JsonRpcException(errorMessage);
            }
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Unable parse a JSON response: " + textResponse, e);
        }
    }

    private String executeRequest() {
        ObjectNode requestNode = request(id, method, params());
        String textRequest;
        String textResponse;
        try {
            textRequest = mapper.writeValueAsString(requestNode);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Unable convert " + requestNode + " to JSON", e);
        }
        try {
            textResponse = transport.pass(textRequest);
        } catch (IOException e) {
            throw new IllegalStateException("I/O error during a request processing", e);
        }
        return textResponse;
    }

    private JsonNode params() {
        if (objectParams.size() > 0) {
            if (arrayParams.size() > 0) {
                throw new IllegalArgumentException("Both object and array params are set");
            }
            return objectParams;
        }
        return arrayParams;
    }

    /**
     * Builds request params as a JSON array
     *
     * @param values request params
     * @return a new JSON array
     */
    private ArrayNode arrayParams(Object[] values) {
        ArrayNode newArrayParams = mapper.createArrayNode();
        for (Object value : values) {
            newArrayParams.add(mapper.valueToTree(value));
        }
        return newArrayParams;
    }

    /**
     * Creates a new JSON-RPC request as a JSON object
     *
     * @param id     request id
     * @param method request method
     * @param params request params
     * @return a new request as a JSON object
     */
    private ObjectNode request(ValueNode id, String method, JsonNode params) {
        if (method.isEmpty()) {
            throw new IllegalArgumentException("Method is not set");
        }
        ObjectNode requestNode = mapper.createObjectNode();
        //requestNode.put(RpcConstants.JSONRPC, RpcConstants.VERSION_2_0);
        requestNode.put(RpcConstants.METHOD, method);
        requestNode.set(RpcConstants.PARAMS, params);
        if (!id.isNull()) {
            requestNode.set(RpcConstants.ID, id);
        }
        return requestNode;
    }
}
