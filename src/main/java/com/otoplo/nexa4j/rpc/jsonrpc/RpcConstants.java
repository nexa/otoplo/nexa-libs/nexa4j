package com.otoplo.nexa4j.rpc.jsonrpc;

public class RpcConstants {

    public static final String VERSION_2_0 = "2.0";
    public static final String RESULT = "result";
    public static final String ERROR = "error";
    public static final String JSONRPC = "jsonrpc";
    public static final String ID = "id";
    public static final String METHOD = "method";
    public static final String PARAMS = "params";
}
