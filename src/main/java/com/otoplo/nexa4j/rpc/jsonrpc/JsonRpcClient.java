package com.otoplo.nexa4j.rpc.jsonrpc;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Date: 8/9/14
 * Time: 8:58 PM
 * <p>
 * JSON-RPC client. Represents a factory for a fluent client API {@link RequestBuilder}.
 * It's parametrized by {@link Transport} and Jackson {@link ObjectMapper}
 */
public class JsonRpcClient {

    /**
     * Transport for performing JSON-RPC requests and returning responses
     */
    private final Transport transport;

    /**
     * JSON mapper for conversion between JSON and Java types
     */
    private final ObjectMapper mapper;

    /**
     * Constructs a new JSON-RPC client with a specified transport
     *
     * @param transport transport implementation
     */
    public JsonRpcClient(Transport transport) {
        this(transport, new ObjectMapper());
    }

    /**
     * Constructs a new JSON-RPC client with a specified transport and user-definder JSON mapper
     *
     * @param transport transport implementation
     * @param mapper    JSON mapper
     */
    public JsonRpcClient(Transport transport, ObjectMapper mapper) {
        this.transport = transport;
        this.mapper = mapper;
    }

    /**
     * Creates a builder of a JSON-RPC request in initial state
     *
     * @return request builder
     */
    public RequestBuilder<Object> createRequest() {
        return new RequestBuilder<>(transport, mapper);
    }
}
