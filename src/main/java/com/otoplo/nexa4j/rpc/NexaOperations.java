package com.otoplo.nexa4j.rpc;

public enum NexaOperations {
    getblock,
    getbalance,
    getnewaddress,
    getblockchaininfo,
    getwalletinfo,
    sendtoaddress,
    validateaddress,
    decoderawtransaction,
    sendrawtransaction
}

