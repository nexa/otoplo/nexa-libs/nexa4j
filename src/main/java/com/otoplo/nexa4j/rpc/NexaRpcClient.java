package com.otoplo.nexa4j.rpc;

import com.otoplo.nexa4j.rpc.jsonrpc.JsonRpcClient;
import com.otoplo.nexa4j.rpc.model.AddressValidationInfo;
import com.otoplo.nexa4j.rpc.model.WalletInfo;
import com.otoplo.nexa4j.rpc.model.Block;
import com.otoplo.nexa4j.rpc.model.transaction.TransactionInfo;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class NexaRpcClient {

    private final JsonRpcClient client;

    public NexaRpcClient(String url, String user, String password) {
        String auth = "Basic " + Base64.getEncoder().encodeToString((user + ":" + password).getBytes());
        client = new JsonRpcClient(request -> Request.Post(url)
                .addHeader(org.apache.http.HttpHeaders.CONTENT_TYPE, ContentType.TEXT_PLAIN.getMimeType())
                .addHeader(org.apache.http.HttpHeaders.AUTHORIZATION, auth)
                .addHeader(org.apache.http.HttpHeaders.CONNECTION, "close")
                .body(new StringEntity(request, StandardCharsets.UTF_8))
                .execute().handleResponse(response -> IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8)));
    }

    public <T> T executeRequest(NexaOperations method, Class<T> returnType, Object... params) {
        return executeRequest(method.toString(), returnType, params);
    }

    public <T> T executeRequest(String method, Class<T> returnType, Object... params) {
        return client.createRequest()
                .method(method)
                .id(System.currentTimeMillis())
                .params(params)
                .returnAs(returnType)
                .execute();
    }

    public Block getBlockInfo(String arg) {
        return executeRequest(NexaOperations.getblock, Block.class, arg, 2);
    }

    public BigDecimal getBalance() {
        return executeRequest(NexaOperations.getbalance, BigDecimal.class);
    }

    public String generateNewAddress() {
        return executeRequest(NexaOperations.getnewaddress, String.class);
    }

    public WalletInfo getWalletInfo() {
        return executeRequest(NexaOperations.getwalletinfo, WalletInfo.class);
    }

    public String sendToAddress(String address, BigDecimal amount) {
        return executeRequest(NexaOperations.sendtoaddress, String.class, address, amount.toPlainString());
    }

    public AddressValidationInfo validateAddress(String address) {
        return executeRequest(NexaOperations.validateaddress, AddressValidationInfo.class, address);
    }

    public TransactionInfo decodeRawTransaction(String hex) {
        return executeRequest(NexaOperations.decoderawtransaction, TransactionInfo.class, hex);
    }

    public String broadcastTransaction(String hex) {
        return executeRequest(NexaOperations.sendrawtransaction, String.class, hex);
    }
}
